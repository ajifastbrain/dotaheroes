//
//  TranslateViewController.swift
//  DotaHeroesX
//
//  Created by Macintosh on 27/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import HWPopController
import L10n_swift


class TranslateViewController: UIViewController {
    @IBOutlet weak var imgCheckInd: UIImageView!
    
    @IBOutlet weak var imgCheckEng: UIImageView!
    
    @IBOutlet weak var btnSave: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.contentSizeInPop = CGSize(width: UIScreen.main.bounds.width, height: 270)
        self.view.backgroundColor = UIColor.clear
        self.btnSave.yellowShadow()
        
        switch L10n.shared.language {
        case Language.indonesian.rawValue:
            imgCheckInd.image = UIImage(named: "atomIcRoundcheck")
            imgCheckEng.image = UIImage(named: "")
        default:
            imgCheckInd.image = UIImage(named: "")
            imgCheckEng.image = UIImage(named: "atomIcRoundcheck")
            break
        }
    }

    @IBAction func selectIndonesia(_ sender: UIButton) {
        switch L10n.shared.language {
        case Language.english.rawValue:
             L10n.shared.language = Language.indonesian.rawValue
        default:
            L10n.shared.language = Language.english.rawValue
            break
        }
        
        // save the preferred language
        UserDefaults.standard.set(L10n.shared.language, forKey: defaultsKeys.LANGUAGE)
        
        imgCheckInd.image = UIImage(named: "atomIcRoundcheck")
        imgCheckEng.image = UIImage(named: "")
    }
    
    @IBAction func selectEnglish(_ sender: Any) {
        switch L10n.shared.language {
        case Language.english.rawValue:
             L10n.shared.language = Language.indonesian.rawValue
        default:
            L10n.shared.language = Language.english.rawValue
            break
        }
        
        // save the preferred language
        UserDefaults.standard.set(L10n.shared.language, forKey: defaultsKeys.LANGUAGE)
        
        imgCheckInd.image = UIImage(named: "")
        imgCheckEng.image = UIImage(named: "atomIcRoundcheck")
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
