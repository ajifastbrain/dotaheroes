//
//  DetailViewController.swift
//  DotaHeroesX
//
//  Created by Macintosh on 25/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import SDWebImage
import L10n_swift


class DetailViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private var listDatas: [DataModel]?
    private var FilteredlistDatas: [DataModel]?
    private var titles = String()
    private var attackType = String()
    private var attribute = String()
    private var health = Int()
    private var maxAttact = Int()
    private var speed = Int()
    private var roles = [String]()
    private var urlImg = String()
    
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var labelTitle: UILabel!
   
    @IBOutlet weak var labelAttackType: UILabel!
    
    @IBOutlet weak var labelPrimaryAttr: UILabel!
    
    @IBOutlet weak var labelHealth: UILabel!
    
    @IBOutlet weak var labelMaxAttact: UILabel!
    
    @IBOutlet weak var labelSpeed: UILabel!
    
    @IBOutlet weak var labelRoles: UILabel!
    
    @IBOutlet weak var healthProgress: UIProgressView!
    
    @IBOutlet weak var maxAttactProgress: UIProgressView!
    
    @IBOutlet weak var speedProgress: UIProgressView!
    
    @IBOutlet weak var mainTable: UICollectionView!
    
    private var errorString: String?
    
    @IBOutlet weak var LAttactType: UILabel!
    
    @IBOutlet weak var LPrimaryAttribute: UILabel!
    
    @IBOutlet weak var LHealth: UILabel!
    
    @IBOutlet weak var LMaxAttact: UILabel!
    
    @IBOutlet weak var LSpeed: UILabel!
    
    @IBOutlet weak var LRoles: UILabel!
    
    @IBOutlet weak var LRelCharacter: UILabel!
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem?.title = ""
        self.title = "Hero Detail"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.labelTitle.text = titles
        self.labelAttackType.text = attackType
        self.labelPrimaryAttr.text = attribute
        self.labelHealth.text = String(health)
        self.labelSpeed.text = String(speed)
        self.labelMaxAttact.text = String(maxAttact)
        
        self.LAttactType.text = "attack_type".l10n()
        self.LPrimaryAttribute.text = "primary_attribute".l10n()
        self.LHealth.text = "health".l10n()
        self.LMaxAttact.text = "maxAttack".l10n()
        self.LSpeed.text = "speed".l10n()
        self.LRoles.text = "roles".l10n()
        self.LRelCharacter.text = "relCharacter".l10n()
        
        mainImage.addShadow()
        mainImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        mainImage.sd_setImage(with: URL(string: "https://api.opendota.com" + urlImg), placeholderImage: nil, options: SDWebImageOptions.retryFailed)
        
        self.healthProgress.setProgress((Float(self.health) / Float(200)), animated: true)
        self.maxAttactProgress.setProgress((Float(self.maxAttact) / Float(50)), animated: true)
        self.speedProgress.setProgress((Float(self.speed) / Float(350)), animated: true)
        self.labelRoles.numberOfLines = 0
        self.labelRoles.lineBreakMode = .byWordWrapping
        self.labelRoles.text = self.roles.joined(separator: ", ")
        
        mainTable.register(UINib(nibName: "RelatedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RelatedCollectionViewCell")
               
        mainTable.delegate = self
        mainTable.dataSource = self
        self.filterPromos(text: attribute)
        mainTable.reloadData()
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 139, height: 174)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing      = 0 // spacing for each item
        mainTable.collectionViewLayout = layout
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let listData = self.FilteredlistDatas {
            return listData.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RelatedCollectionViewCell", for: indexPath as IndexPath) as! RelatedCollectionViewCell
        let URLImg: String = FilteredlistDatas?[indexPath.row].img ?? ""
        let titles: String = FilteredlistDatas?[indexPath.row].localized_name ?? ""
        cell.configureView(imgURL: URLImg, name: titles)
        
        return cell
    }

    func filterPromos(text: String) {
        if text.length() > 0 {
            self.errorString = nil
            
            self.FilteredlistDatas = self.listDatas?.filter({ (elm) -> Bool in
                (elm.primary_attr?.lowercased().contains(text.lowercased()))!
            })
            if self.FilteredlistDatas?.count == 0 {
                self.errorString = "NO STORE MATCH YOUR QUERY"
            }
        } else {
            self.FilteredlistDatas = self.listDatas
        }
        
        _ = self.FilteredlistDatas?.sorted {
            var isSorted = false
            if let first = $0.localized_name, let second = $1.localized_name {
                isSorted = first < second
            }
            return isSorted
        }
    }


    convenience init(listData: [DataModel]?, title: String, attackType: String, attribute: String, health: Int, maxAttact: Int, speed: Int, roles: [String], mainImg: String) {
        self.init()
        self.listDatas = listData
        self.FilteredlistDatas = listData
        self.titles = title
        self.attackType = attackType
        self.attribute = attribute
        self.health = health
        self.maxAttact = maxAttact
        self.speed = speed
        self.roles = roles
        self.urlImg = mainImg
    }

}
