//
//  HomeViewController.swift
//  DotaHeroes
//
//  Created by Macintosh on 23/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import ObjectMapper
import HWPopController


class HomeViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    private var homeListModels: [DataModel]?
    
    private var filteredListModels: [DataModel]?
    
    @IBOutlet weak var mainTable: UICollectionView!
    
    @IBOutlet weak var txtSearch: UITextField!
    
    private var errorString: String?
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Main Menu"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Lang", style: .plain, target: self, action: #selector(changeLang))
        
        mainTable.register(UINib(nibName: "ListImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ListImageCollectionViewCell")
               
        mainTable.delegate = self
        mainTable.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 128, height: 220)
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing      = 0 // spacing for each item
        mainTable.collectionViewLayout = layout
        
        txtSearch.layer.cornerRadius = 23
        txtSearch.layer.borderWidth = 1
        txtSearch.layer.borderColor = UIColor.init(hexString: "#D0CCCC").cgColor
        txtSearch.setLeftPaddingPoints(50)
        txtSearch.setRightPaddingPoints(16)
        txtSearch.addTarget(self, action: #selector(onSearch(sender:)), for: .editingChanged)
                      

      
        self.showLoadingView()
        API.getHomeList(completion: { (dataModelList, error) in
             if let dataModel = dataModelList {
                self.homeListModels = dataModel
                self.filteredListModels = dataModel
                self.mainTable.reloadData()
                 self.hideLoadingView()
             } else if let error = error {
                print(error)
                self.hideLoadingView()
             }
        })

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let listData = self.filteredListModels {
            return listData.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListImageCollectionViewCell", for: indexPath as IndexPath) as! ListImageCollectionViewCell
        let URLImg: String = filteredListModels?[indexPath.row].img ?? ""
        let titles: String = filteredListModels?[indexPath.row].localized_name ?? ""
        cell.configureView(imgURL: URLImg, name: titles)
        
        return cell
    }
    
    @objc func onSearch (sender: UITextField) {
        self.filterPromos(text: sender.text ?? "")
        self.mainTable.reloadData()
    }

    func filterPromos(text: String) {
        if text.length() > 0 {
            self.errorString = nil
            
            self.filteredListModels = self.homeListModels?.filter({ (elm) -> Bool in
                (elm.localized_name?.lowercased().contains(text.lowercased()))! || (elm.name?.lowercased().contains(text.lowercased()))!
            })
            if self.filteredListModels?.count == 0 {
                self.errorString = "NO STORE MATCH YOUR QUERY"
            }
        } else {
            self.filteredListModels = self.homeListModels
        }
        
        _ = self.filteredListModels?.sorted {
            var isSorted = false
            if let first = $0.localized_name, let second = $1.localized_name {
                isSorted = first < second
            }
            return isSorted
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let URLImg: String = filteredListModels?[indexPath.row].img ?? ""
        let titles: String = filteredListModels?[indexPath.row].localized_name ?? ""
        let attackType: String = filteredListModels?[indexPath.row].attack_type ?? ""
        let attribute: String = filteredListModels?[indexPath.row].primary_attr ?? ""
        let health: Int = filteredListModels?[indexPath.row].base_health ?? 0
        let maxAttact: Int = filteredListModels?[indexPath.row].base_attack_max ?? 0
        let speed: Int = filteredListModels?[indexPath.row].move_speed ?? 0
        let roles: [String] = filteredListModels?[indexPath.row].roles ?? [""]
        
        let controller = DetailViewController(listData: filteredListModels, title: titles, attackType: attackType, attribute: attribute, health: health, maxAttact: maxAttact, speed: speed, roles: roles, mainImg: URLImg)
        controller.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func changeLang() {
        let bottomAuthVC = TranslateViewController()
        let popController = HWPopController(viewController: bottomAuthVC)
        popController.containerView.layer.cornerRadius = 20
        popController.containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        popController.popPosition = .bottom
        popController.popType = .bounceInFromBottom
        popController.dismissType = .bounceOutToBottom
        popController.shouldDismissOnBackgroundTouch = false
        popController.present(in: self)
    }
}
