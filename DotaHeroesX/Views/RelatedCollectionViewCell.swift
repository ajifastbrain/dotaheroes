//
//  RelatedCollectionViewCell.swift
//  DotaHeroesX
//
//  Created by Macintosh on 26/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import SDWebImage

class RelatedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelTitle.font = UIFont.appFont(fontType: FontType.bold, fontSize: FontSize.SSS)
        labelTitle.textColor = UIColor.black
        mainImage.addShadow()
    }

    func configureView(imgURL: String, name: String) {
        labelTitle.text = name
        mainImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        mainImage.sd_setImage(with: URL(string: "https://api.opendota.com" + imgURL), placeholderImage: nil, options: SDWebImageOptions.retryFailed)
    }
}
