//
//  ListImageCollectionViewCell.swift
//  DotaHeroes
//
//  Created by Macintosh on 23/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import SDWebImage

class ListImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var mainImg: UIImageView!
    
    @IBOutlet weak var labelName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelName.font = UIFont.appFont(fontType: FontType.bold, fontSize: FontSize.SS)
        labelName.textColor = UIColor.white
        mainImg.addShadow()
    }
    
    func configureView(imgURL: String, name: String) {
        labelName.text = name
        mainImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        mainImg.sd_setImage(with: URL(string: "https://api.opendota.com" + imgURL), placeholderImage: nil, options: SDWebImageOptions.retryFailed)
    }

}
