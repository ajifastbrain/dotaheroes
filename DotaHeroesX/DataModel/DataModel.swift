//
//  DataModel.swift
//  DotaHeroes
//
//  Created by Macintosh on 23/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import ObjectMapper

class DataModel: Mappable {
    var id: Int?
    var name: String?
    var localized_name: String?
    var primary_attr: String?
    var attack_type: String?
    var roles: [String]?
    var img: String?
    var icon: String?
    var base_health: Int?
    var base_health_regen: Double?
    var base_mana: Int?
    var base_mana_regen: Int?
    var base_armor: Int?
    var base_mr: Int?
    var base_attack_min: Int?
    var base_attack_max: Int?
    var base_str: Int?
    var base_agi: Int?
    var base_int: Int?
    var str_gain: Double?
    var agi_gain: Double?
    var int_gain: Double?
    var attack_range: Int?
    var projectile_speed: Int?
    var attack_rate: Double?
    var move_speed: Int?
    var turn_rate: Double?
    var cm_enabled: Bool?
    var legs: Int?
    var pro_ban: Int?
    var hero_id: Int?
    var pro_win: Int?
    var pro_pick: Int?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        localized_name <- map["localized_name"]
        primary_attr <- map["primary_attr"]
        attack_type <- map["attack_type"]
        roles <- map["roles"]
        img <- map["img"]
        icon <- map["icon"]
        base_health <- map["base_health"]
        base_health_regen <- map["base_health_regen"]
        base_mana <- map["base_mana"]
        base_mana_regen <- map["base_mana_regen"]
        base_armor <- map["base_armor"]
        base_mr <- map["base_mr"]
        base_attack_min <- map["base_attack_min"]
        base_attack_max <- map["base_attack_max"]
        base_str <- map["base_str"]
        base_agi <- map["base_agi"]
        base_int <- map["base_int"]
        str_gain <- map["str_gain"]
        agi_gain <- map["agi_gain"]
        int_gain <- map["int_gain"]
        attack_range <- map["attack_range"]
        projectile_speed <- map["projectile_speed"]
        attack_rate <- map["attack_rate"]
        move_speed <- map["move_speed"]
        turn_rate <- map["turn_rate"]
        cm_enabled <- map["cm_enabled"]
        legs <- map["legs"]
        pro_ban <- map["pro_ban"]
        hero_id <- map["hero_id"]
        pro_win <- map["pro_win"]
        pro_pick <- map["pro_pick"]
        
    }
}
