//
//  BaseViewController.swift
//  DotaHeroes
//
//  Created by Macintosh on 22/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(statusManager),
                         name: .flagsChanged,
                         object: nil)
        updateUserInterface()
        

        self.updateUserInterface()
    }
    
    func showLoadingView() {
           if let window = UIApplication.shared.keyWindow {
               LoadingOverlay.shared.showOverlay(inView: window)
           } else {
               LoadingOverlay.shared.showOverlay(inView: self.navigationController!.view)
           }
       }
       
       func hideLoadingView() {
           LoadingOverlay.shared.hideOverlayView()
       }

    public func showAlertWith(title: String, message: String, action: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK".uppercased(), style: UIAlertAction.Style.cancel, handler: { (alertAction) in
            if let action = action {
                action()
            }
        })
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func navigationBarTitle(withTitle: String) {
        let titleLabel = UILabel(frame: .zero)
        titleLabel.textColor = UIColor.black
        titleLabel.numberOfLines = 0
        titleLabel.text = ""
        
        
        titleLabel.text = withTitle
        titleLabel.font = UIFont.appFont(fontType: FontType.bold, fontSize: FontSize.L)
        titleLabel.sizeToFit()
        self.navigationItem.titleView = titleLabel
    }
    
    func updateUserInterface() {
        switch Network.reachability.status {
        case .unreachable:
            let alertController = UIAlertController(title: "Warning", message: "No Internet Connection", preferredStyle: .alert)
            let actionOK = UIAlertAction(title: "OK", style: .default) { _ in self.closeAction() }
            alertController.addAction(actionOK)
            self.present(alertController, animated: true, completion: nil)
        case .wwan:
            print("WWAN is Active")
        case .wifi:
            print("WIFI is Active")
        }
        print("Reachability Summary")
        print("Status:", Network.reachability.status)
        print("HostName:", Network.reachability.hostname ?? "nil")
        print("Reachable:", Network.reachability.isReachable)
        print("Wifi:", Network.reachability.isReachableViaWiFi)
    }
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    @objc private func closeAction() {
      dismiss(animated: true, completion: nil)
    }
    
}
