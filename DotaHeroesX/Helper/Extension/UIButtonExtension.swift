//
//  UIButtonExtension.swift
//  DotaHeroesX
//
//  Created by Macintosh on 28/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func yellowShadow() {
        layer.shadowColor = UIColor(hexString: "#fec326").cgColor
        
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 8
        layer.shadowOffset = CGSize(width: 0, height: 6)
        
        layer.masksToBounds = false
        layer.cornerRadius = 25
        backgroundColor = UIColor(hexString: "#fec326")
    }
}
