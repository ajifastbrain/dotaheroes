//
//  FontExtension.swift
//  DotaHeroes
//
//  Created by Macintosh on 22/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

enum FontSize: CGFloat {
    case ENORMOUS = 45
    case XXXXL = 35
    case XXXL = 26
    case XXL = 24
    case XL = 22
    case LLL = 20
    case LL = 18
    case L = 17
    case M = 16
    case S = 15
    case SS = 13
    case SSS = 11
    
    static func getPreferredFontSize(defaultSize: CGFloat) -> CGFloat {
        let defaultFontSize = defaultSize
        
        // IN CASE YOU NEED DIFFERENT FONT SIZE PER DEVICE, SET HERE 😉
        switch UIScreen.deviceType() {
        case .iPhone4:
            return defaultFontSize
        case .iPhone5:
            return defaultFontSize
        case .iPhone6P:
            return defaultFontSize
        default:
            return defaultFontSize
        }
    }
}

enum FontType: String {
    case extraLight = "Muli-ExtraLight"
    case light = "Muli-Light"
    case regular = "Muli-Regular"
    case medium = "Muli-Medium"
    case semiBold = "Muli-SemiBold"
    case bold = "Muli-Bold"
    case extraBold = "Muli-ExtraBold"
    case black = "Muli-Black"
    case extraLightItalic = "Muli-ExtraLightItalic"
    case lightItalic = "Muli-LightItalic"
    case italic = "Muli-Italic"
    case mediumItalic = "Muli-MediumItalic"
    case semiBoldItalic = "Muli-SemiBoldItalic"
    case bolItalic = "Muli-BoldItalic"
    case extraBoldItalic = "Muli-ExtraBoldItalic"
    case boldItalic = "Muli-BlackItalic"
    
}


extension UIFont {
    class func appFont(fontType: FontType, fontSize: FontSize) -> UIFont {
        return UIFont(name: fontType.rawValue, size: FontSize.getPreferredFontSize(defaultSize: fontSize.rawValue))!
    }
}
