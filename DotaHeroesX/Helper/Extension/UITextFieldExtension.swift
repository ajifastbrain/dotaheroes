//
//  UITextFieldExtension.swift
//  DotaHeroes
//
//  Created by Macintosh on 24/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

private var __maxLengths = [UITextField: Int]()

extension UITextField {

    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }

    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }

    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }

    func customBorder() {
        self.font = UIFont.appFont(fontType: FontType.regular, fontSize: FontSize.M)
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.init(hexString: "#CED4DA").cgColor
            
        self.setLeftPaddingPoints(15)
        self.setRightPaddingPoints(70)
        self.leftViewMode = UITextField.ViewMode.always
    }
}
