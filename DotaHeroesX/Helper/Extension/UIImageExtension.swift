//
//  UIImageExtension.swift
//  DotaHeroesX
//
//  Created by Macintosh on 26/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func addShadow() {
        // corner radius
        layer.cornerRadius = 10

        // border
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.black.cgColor

        // shadow
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1.0)
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 4.0
    }
}
