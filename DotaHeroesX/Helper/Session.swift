//
//  Session.swift
//  DotaHeroes
//
//  Created by Macintosh on 22/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import Alamofire

class Session: NSObject {
    static var sharedInstance: Session {
        struct Static {
            static let instance = Session()
        }
        
        return Static.instance
    }
}
