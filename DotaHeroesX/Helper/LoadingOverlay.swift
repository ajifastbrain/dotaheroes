//
//  LoadingOverlay.swift
//  DotaHeroes
//
//  Created by Macintosh on 22/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

public class LoadingOverlay {
    var overlayView = UIView()
    var indicatorImageView = UIImageView()
    
    class var shared: LoadingOverlay {
        struct Static {
            static let instance: LoadingOverlay = LoadingOverlay()
        }
        return Static.instance
    }
    
    public func showOverlay(inView view: UIView) {
        if !overlayView.isDescendant(of: view) {
            overlayView = UIView(frame: UIScreen.screenBounds)
            overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            indicatorImageView                  = UIImageView(frame: CGRect(x: 0, y: 0, width: 33, height: 33))
            indicatorImageView.image            = UIImage(named: "loaders")
            indicatorImageView.backgroundColor  = UIColor.clear
            indicatorImageView.center           = overlayView.center
            
            let rotationAnim = CABasicAnimation(keyPath: "transform.rotation")
            rotationAnim.fromValue = 0.0
            rotationAnim.toValue = Double.pi * 2.0
            rotationAnim.duration = 1.0
            rotationAnim.repeatCount = .infinity
            indicatorImageView.layer.add(rotationAnim, forKey: nil)
            
            overlayView.addSubview(indicatorImageView)
            
            view.isUserInteractionEnabled = false
            view.addSubview(overlayView)
        }
    }
    
    public func hideOverlayView() {
        overlayView.superview?.isUserInteractionEnabled = true
        overlayView.removeFromSuperview()
    }
}
