//
//  Language.swift
//  DotaHeroesX
//
//  Created by Macintosh on 27/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

enum Language: String {
    case english = "en"
    case indonesian = "id"
}
