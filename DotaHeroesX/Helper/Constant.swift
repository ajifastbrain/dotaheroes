//
//  Constant.swift
//  DotaHeroes
//
//  Created by Macintosh on 22/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

struct defaultsKeys {
    static let keyLogin = "keyLogin"
    static let LANGUAGE = "language"
}

struct DotaHeroesNotification {
    static let tokenExpired     = "NOTIFICATION_TOKEN_EXPIRED"
    static let pinChanged       = "NOTIFICATION_PIN_CHANGED"
}

struct APIEndPoint {
    static let home         = "/herostats"
}
