//
//  Strings.swift
//  DotaHeroes
//
//  Created by Macintosh on 22/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

struct Localization {
    struct Label {
        static let LOGIN = "Login".localized
    }
}
