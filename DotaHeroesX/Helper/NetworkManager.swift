//
//  NetworkManager.swift
//  DotaHeroes
//
//  Created by Macintosh on 22/10/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {
    
    class func request(_ method: HTTPMethod, _ URLString: URLConvertible, parameters: [String: AnyObject]? = nil, encoding: ParameterEncoding = URLEncoding.default, useBasicToken: Bool? = false, logoutIf401: Bool = true, completion: @escaping (DataResponse<Any>) -> Void) {
        
        let manager = Alamofire.SessionManager.default
        let headers = [String : String]()
        
        let req = manager.request(URLString, method: method, parameters: parameters, encoding: encoding, headers: headers)
        req.responseString { (response) in
            if let response = response.response {
                Logger.log(response)
                if response.statusCode == 401 && logoutIf401
                {
                    NotificationCenter.default.post(name: NSNotification.Name(DotaHeroesNotification.tokenExpired), object: nil)
                    
                    // USER SIGN OUT TODO
                    print("User Sign Out TODO")
                    return
                    
                } else {
                    req.responseJSON { (response) -> Void in
                        completion(response)
                    }
                }
            } else {
                req.responseJSON { (response) -> Void in
                    completion(response)
                }
            }
        }
    }
}
